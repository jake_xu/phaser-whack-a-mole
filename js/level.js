MoleGame.level = function (game) {
};

MoleGame.level.prototype = {

  create: function() {
    this.background = this.add.sprite(0,0, 'garden');
    //score
    this.molesWhacked = 0;
    this.countdownSeconds = 10;
    enemies = this.add.group();
    this.scoreText = this.add.text(20, 20, "SCORE:", { font: "35px Arial", fill: "#000" });  
    this.labelScore = this.add.text(160, 10, this.molesWhacked, { font: "50px Arial", fill: "#000" });  
    this.newMole();
    this.molespawnTimer = this.time.events.loop(Phaser.Timer.SECOND * 2, this.newMole, this);
    this.molespawnTimer.timer.start();
    //Game Countdown
    this.countdown = this.add.text(750, 20, this.countdownSeconds, { font: "60px Arial", fill: "red" });  
    this.countdownTimer = this.time.events.loop(Phaser.Timer.SECOND * 1, this.countdowntick, this);
    this.countdownTimer.timer.start();
  },

  countdowntick: function() {
    if (this.countdownSeconds == 1 ) {
      game.state.start('gameOver', true, false, this.molesWhacked);
    }
    this.countdownSeconds--;
    this.countdown.text = this.countdownSeconds; 
  },

  killMole: function(sprite, event) {
    sprite.animations.play('whacked');
    sprite.events.onAnimationComplete.add(function(){
      sprite.destroy();
    }, this);
    this.scoreUp();
  },

  newMole: function() {
    for (i = 0; i < 5; i++) {
      newx = Math.floor(Math.random() * 800) + 1;
      newy = Math.floor(Math.random() * 450) + 75;
      var mole = enemies.create(newx, newy, 'mole');
      mole.animations.add('appear', [1,2,3,0], 8, false);
      mole.animations.add('hide', [0,3,2,1], 8, false);
      mole.animations.add('whacked', [4], 5, false);
      mole.animations.play('appear');
      mole.inputEnabled = true;
      mole.events.onInputDown.add(this.killMole, this);
      timer = game.time.create(false);
      timer.add(2000, this.moleHide, mole);
      mole.timerInstance = timer;
      timer.start();
    }
  },

  scoreUp: function() {
  this.molesWhacked++;
  this.labelScore.text = this.molesWhacked; 
  },

  moleHide: function() {
    this.animations.play('hide');
    this.events.onAnimationComplete.add(function(){
      this.destroy();
    }, this);
  }

}



