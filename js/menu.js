var MoleGame = {};

MoleGame.menu = function (game) {
};

MoleGame.menu.prototype = {

  preload: function() {
    this.load.image('garden', 'images/garden.jpg');   
    this.load.spritesheet('mole', 'images/mole.png', 100, 76, 5, 0, 0);
  },

  create: function() {
    this.background = this.add.sprite(0,0, 'garden');
    startText = this.add.text(20, 20, "START", { font: "35px Arial", fill: "red" });  
    startText.inputEnabled = true;
    startText.events.onInputDown.add(this.startGame, this);
  },

  startGame: function() {
    console.log('test');
        game.state.start('level');
  }

}



